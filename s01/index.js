let students = [
	"John",
	"Jane",
	"Jeff",
	"Kevin",
	"David"
];

console.log(students);

class Dog {
	constructor(name, breed, age){
		this.name = name;
		this.breed = breed;
		this.age = age;
	}
}
let dog1 = new Dog("Bantay","Corgi",3);

console.log(dog1);

let person1 = {
	name: "Saitama",
	heroName: "One Punch Man",
	age: 30
};

console.log(person1);

/* 
				Primitives
Date types that do not have properties nor methods

				JS Primitives
	1. String
	2. Number
	3. Boolean
	4. Null
	5. Undefined
*/

/*
	why can you use methods in primitive data types?

	they are temporarily converted to an object to perform operation called
*/

// what array method can we use to add an items at the end of an array?
students.push("Henry");

console.log(students);

// What array method can we use to add add item at the start of an array?
students.unshift("Jeremiah");
console.log(students);

students.unshift("Christine");
console.log(students);

// What array method does the opposite of push()?
students.pop();
console.log(students);

// What array method does the opposite of unshift()

students.unshift();
console.log(students);

// What is the difference between splice() and slice()?
// .splice() allows us to remove and add items from a starting index (Mutator method)
// .slice() copies a portion from starting index and returns a new array from it (Non-mutator)

// What is another kind of array method?
// Iterator methods loops over the items of an array

// forEach() - loops over items in an array and repeats a user-defined function

// map() - loops over items in an array and repeats a user-defined function AND returns a new array

// every() - loops and checks if all items satisfies a given condition and returns a boolean

let arrNum = [15,25,50,20,10,11];

// check if every item in arrNum is divisible by 5

let allDivisible;
arrNum.forEach(num => {
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5.`)
	} else {
		allDivisible = false;
	}
	// However, can forEach() return data that will tell us IF all numbers/items in our arrNum array is divisible by 5?

})

console.log(allDivisible);

// arrNum.pop();
// arrNum.push(35);
let divisibleBy5 = arrNum.every(num => {

	// Every is able to return data.
	// When the function inside every() is able to return true, the loop with continue until all items are able to return true. Else, IF at least one item returns false, then the loop will stop there and every() will return false.

	console.log(num);
	return num % 5 === 0;

})
console.log(divisibleBy5);

// some() - loops and check if at least one item satifies a given condition and returns a boolean. It will return true if at least ONE item satifies the condition, else IF no item returns true/satisfies the condition, some() will return false

let someDivisibleBy5 = arrNum.some(num => {
	
	// some() will stop its loop once a function/item is able to return true.
	// then, some() will return else, if all items does not satisfy the condition or return true, some() will return false

	console.log(num);
	return num % 5 === 0
});

console.log(someDivisibleBy5);

arrNum.push(35);
let someDivisibleBy6 = arrNum.some(num => {

	console.log(num);
	return num % 6 === 0;
});

console.log(someDivisibleBy6);


/* 	QUIZ
	1. we create arrays using []
	2. arrName[0]
	3. arrName[arrName.length - 1]
	4. indexOf()
	5. forEach()
	6. map()
	7. every()
	8. some()
	9. false
	10. true
	11. false
	12. Math
	13. false
	14. true
	15. true
*/

// ACTIVITY

// 1
function addToEnd(students, newStudent){
	if(typeof(newStudent) === 'string'){
		students.push(newStudent);
		return students
	} else {
		return console.log("error - can only add strings to an array");
	}
}

// 2
function addToStart(students, newStudent){
	if(typeof(newStudent) === 'string'){
		students.unshift(newStudent);
		return students
	} else {
		return console.log("error - can only add strings to an array");
	}
}

// 3
function elementChecker(students, element){
	if(students.includes(element)){
		return true
	} else if (students.length === 0){
		return "error - passed in array is empty"
	} else {
		return false
	}
}

// 4
function stringLengthSorter(students){

	let stringCheck = students.every(student => {
		return typeof(student) === 'string'
	});

	if (stringCheck === false){
		return "error- all array elements must be strings"
	} else {
		return students.sort((a,b) => a.length - b.length)
	}
}